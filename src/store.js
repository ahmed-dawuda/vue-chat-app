import Vue from 'vue'
import Vuex from 'vuex'
// import socket from './io-client'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
      user: {
          oldPassword: '',
          newPassword: ''
      },
      posts: null,
      socket: null
    },
    getters: {
        getUser (state) {
            return state.user
        },
        getPosts (state) {
            return state.posts
        },
        getSocket (state) {
            return state.socket
        }
    },
    mutations: {
      setUser (state, payload) {
        if (payload.image.charAt(0) === '/') {
            payload.image = `http://localhost:3000${payload.image}`
        } 
        state.user = payload
      },
      setPosts (state, payload) {
          state.posts = payload
      },
      pushPost (state, payload) {
          state.posts.unshift(payload)
      },
      setImage (state, payload) {
          state.user.image = payload
      }
    },
    actions: {
        setUser ({commit}, payload) {
            commit('setUser', payload)
        },
        setPosts ({commit}, payload) {
            commit('setPosts', payload)
        },
        pushPost ({commit, state}, payload) {
            const post = {
                author: {
                    username: state.user.username
                },
                postedOn: (new Date()).toDateString(),
                content: payload
            }
            // 'http://localhost:3000'
            post.author.image = state.user.image.substr(21, state.user.image.lenght)
            // console.log(post)
            commit('pushPost', post)
            return post
        },
        setImage ({commit}, payload) {
            commit('setImage', payload)
        }
    }
  })

  export default store