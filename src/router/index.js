import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/login.vue'
import Register from '../components/register.vue'
import Dashboard from '../components/dashboard.vue'
import Profile from '../components/profile.vue'
import Room from '../components/room.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
      meta: {visitorOnly: true}
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {visitorOnly: true}
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {auth: true},
      children: [
        {
          name: 'profile',
          path: 'profile',
          component: Profile,
          meta: {auth: true}
        },
        {
          name: 'room',
          path: 'chat-room',
          component: Room,
          meta: {auth: true}
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    let token = localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next({name: 'login'})
    }
  } else {
    next()
  }
})

export default router