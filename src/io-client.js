import io from 'socket.io-client'
import store from './store'

export default () => {

    // console.log(store.)

    const socket = io('http://localhost:3000')

    socket.on('connect', () => {
        console.log(socket.id)
    })

    socket.on('welcome', function(data) {
        console.log(data)
    })

    socket.on('hello', function(data) {
        console.log(data)
    })

    socket.on('recieve-chat-room-message', function(data) {
        store.state.posts.unshift(data)
    })

    store.state.socket = socket

    return socket;
}