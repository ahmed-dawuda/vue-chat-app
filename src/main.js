import Vue from 'vue'
import App from './App'
import router from './router'
import VeeValidate from 'vee-validate'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'

Axios.defaults.baseURL = 'http://localhost:3000/'
// Axios.interceptors.request.use()
Axios.interceptors.request.use((config) => {
  let apiToken = localStorage.getItem('token')
  if (apiToken && !config.headers.common.Authorization) {
    config.headers.common.Authorization = `Bearer ${apiToken}`
  }
  return config
})

Vue.use(VueAxios, Axios)
Vue.config.productionTip = false
Vue.use(VeeValidate)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
